﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ
{
    class Payment
    {
        public Employee employee { get; set; }
        public DateTime date { get; set; }
        public int amount { get; set; }

        public Payment(Employee employee, DateTime date, int amount)
        {
            this.employee = employee;
            this.date = date;
            this.amount = amount;
        }
    }
}
