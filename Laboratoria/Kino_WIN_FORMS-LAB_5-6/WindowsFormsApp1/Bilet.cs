﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Bilet
    {
        public string movie { get; set; }
        public string ticketType { get; set; }

        public Bilet(string movie, string ticketType)
        {
            this.movie = movie;
            this.ticketType = ticketType;
        }
    }
}
