﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struktury_i_Klasy_generyczne
{
    class Klient
    {
        public string name { get; set; }
        public string surname { get; set; }

        public Klient()
        {
        }

        public Klient(string name, string surname)
        {
            this.name = name;
            this.surname = surname;
        }

        public void Wyswietl()
        {
            Console.WriteLine("Imię: " + name + " Nazwisko: " + surname);
        }
    }
}
