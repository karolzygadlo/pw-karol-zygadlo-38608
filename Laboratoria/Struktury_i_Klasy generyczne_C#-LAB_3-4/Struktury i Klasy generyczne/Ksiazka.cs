﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struktury_i_Klasy_generyczne
{
    class Ksiazka
    {
        public string name { get; set; }
        public string author { get; set; }
        public double price { get; set; }

        public Ksiazka(string name, string author, double price)
        {
            this.name = name;
            this.author = author;
            this.price = price;
        }

        public void Wyswietl()
        {
            Console.WriteLine("Tytuł: " + name + " Autor: " + author + " Cena: " + price);
        }
    }
}
